console.log(`S23_Activity Postman and REST API`)
// https://jsonplaceholder.typicode.com/todos
/*
	1. In the S33 folder, create an activity folder and an index.html and a script.js file inside of it.
	2. Link the index.js file to the index.html file.
	3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
	4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
*/
			        async function toDo() {
			            let result = await fetch("https://jsonplaceholder.typicode.com/todos");
			            let json = await result.json();
			            let array = json.map(function(json){
			            	return json.title;
			            });
			            console.log(array)
			        }
			        toDo();  

        

/*
	5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
	6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
*/
					fetch("https://jsonplaceholder.typicode.com/todos/1")
					.then(response => response.json())
					.then(json => { 
						console.log(json)
						console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
					});

/*
	7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
*/
					fetch("https://jsonplaceholder.typicode.com/todos/", {
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
							body: JSON.stringify({
							title:"create a to do list item",
							completed: false,
							userId: 1
						})
					})
					.then (response => response.json())
					.then (json => console.log (json))
/*
	8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
	9. Update a to do list item by changing the data structure to contain the following properties:
	- Title
	- Description
	- Status
	- Date Completed
	- User ID
*/
					fetch("https://jsonplaceholder.typicode.com/todos/1", {
						method: "PUT",
						headers: {
							"Content-Type": "application/json"
						},
							body: JSON.stringify({
							title:"Updated a to do list item",
							description: "To update the my to do list with a different data structure",
							status: "Pending",
							dateCompleted: "Pending",
							userId: 1
						})
					})
					.then (response => response.json())
					.then (json => console.log (json))
		
/*
	10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
	11. Update a to do list item by changing the status to complete and add a date when the status was changed.
*/
					fetch("https://jsonplaceholder.typicode.com/todos/1", {
						method: "PATCH",
						headers: {
							"Content-Type": "application/json"
						},
							body: JSON.stringify({
							title:"delectus aut autem",
							status: "Complete",
							dateCompleted: "07/09/2021",
							userId: 1
						})
					})
					.then (response => response.json())
					.then (json => console.log (json))

/*
	12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
*/

					fetch("https://jsonplaceholder.typicode.com/todos/1", {
						method: "DELETE"
					})
					.then (response => response.json())
					.then (json => console.log (json))
/*

	13. Create a request via Postman to retrieve all the to do list items.
	- GET HTTP method
	- https://jsonplaceholder.typicode.com/todos URI endpoint
	- Save this request as get all to do list items
	14. Create a request via Postman to retrieve an individual to do list item.
	- GET HTTP method
	- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	- Save this request as get to do list item
	15. Create a request via Postman to create a to do list item.
	- POST HTTP method
	- https://jsonplaceholder.typicode.com/todos URI endpoint
	- Save this request as create to do list item
	16. Create a request via Postman to update a to do list item.
	- PUT HTTP method
	- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	- Save this request as update to do list item PUT
	- Update the to do list item to mirror the data structure used in the PUT fetch request
	17. Create a request via Postman to update a to do list item.
	- PATCH HTTP method
	- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	- Save this request as update to do list item PATCH
	- Update the to do list item to mirror the data structure of the PATCH fetch request
	18. Create a request via Postman to delete a to do list item.
	- DELETE HTTP method
	- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	- Save this request as delete to do list item
	19. Export the Postman collection and save it in the activity folder.
	20. Create a git repository named S33.
	21. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	22. Add the link in Boodle.
*/